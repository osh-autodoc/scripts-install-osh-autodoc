<!--
SPDX-FileCopyrightText: 2022 Pieter Hijma <info@pieterhijma.net>

SPDX-License-Identifier: Apache-2.0
-->

# Scripts to install OSH Automated Documentation

## Introduction

This repository contains simple scripts to install the software needed for OSH
Automated Documentation.  See [OSH Automated Documentation
website](https://osh-autodoc.org) for more information on how to use these
scripts.

## Prerequisites

The scripts assume `curl`, `jq`, and `tar` to be available besides the programs
that are required to install FreeCAD and the PDF compiler.

## License

These scripts are licensed under APACHE-2.0.  See the LICENSES directory and
SPDX metadata for more details.  The project is
[REUSE](https://reuse.software/) compliant.

## Authors

- Pieter Hijma

